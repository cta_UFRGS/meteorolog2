Projeto Meteorolog2
===================

Mais informações sobre o projeto em https://cta.if.ufrgs.br/projects/estacao-meteorologica-modular/wiki/Meteorolog2

Licença
-------

Os programas do projeto Meteorolog2 estão disponíveis sob os termos da licença GNU AFFERO GENERAL PUBLIC LICENSE 3.0 (AGPL 3.0) ou posterior, mantida pela Free Software Foundation. Os termos da licença podem ser encontrados no arquivo agpl-3.0.txt

