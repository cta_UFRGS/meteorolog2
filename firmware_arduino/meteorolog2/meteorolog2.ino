#include <EEPROM.h>
#include <Wire.h>
#include <RTClib.h>
#include <Sensirion.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP085.h>
#include <SD.h>


#define PIN_PLUV 2
#define PIN_HTEMP_DATA 6
#define PIN_HTEMP_CLK 7
#define PIN_DEBUG 12 //Se = HIGH com DEBUG, Se = LOW sem DEBUG
#define PIN_LDR 0 
#define PIN_PLUVIOMETRO 2 //Pino que o pluviometro esta ligado
#define INT_PLUVIOMETRO 0 //Interrupcao que o pluviometro esta ligado
#define PIN_SD_CS 8 //Pino Chip Select do cartao SD
#define PIN_RELE_GPRS 5 //Pino que controla a energizacao do modem GPRS

#define RELE_LIGADO LOW
#define RELE_DESLIGADO HIGH

boolean DEBUG;
boolean DEBUG_SOFT_FLAG = false; //Flag que habilita DEBUG via comando Serial
word PERIODO_MEDICAO = 300; //300 segundos por default (5 minutos)

boolean sensorSHT, sensorPluv, sensorLDR, sensorBMP; //EEPROM(2), bits 0,1,2,3 respectivamente
boolean logado = false;
String dataHoraAtual;

  unsigned int hTempData=0;
  float temperatura=0.0;
  float umidade=0.0;
  int luminosidade=0;
  float bmpTemperatura=0.0;
  long bmpPressao=0;

long auxFuturo = 0;
volatile word contadorPluviometro=0;
volatile unsigned long pluvDebounce=0;

RTC_DS1307 rtc;
Sensirion sht=Sensirion(PIN_HTEMP_DATA, PIN_HTEMP_CLK);
Adafruit_BMP085 bmp;
File root;
void setup(){
  pinMode(PIN_DEBUG,INPUT); //PIN_DEBUG como entrada
  pinMode(PIN_PLUV,INPUT); //PIN_PLUV como entrada
  pinMode(PIN_SD_CS,OUTPUT);
  pinMode(PIN_RELE_GPRS,OUTPUT);
  
  digitalWrite(PIN_DEBUG,HIGH); //PIN_DEBUG com pull-up
  digitalWrite(PIN_PLUV,HIGH); //PIN_PLUV com pull-up
  digitalWrite(PIN_RELE_GPRS,RELE_LIGADO); //habilita GPRS
  DEBUG = digitalRead(PIN_DEBUG);  
  
  noInterrupts(); //Desabilita interrupcoes para evitar contagem do pluviometro acidental na energizaço do equipamento
  attachInterrupt(INT_PLUVIOMETRO, incrementaPluviometro, RISING);//interrupço externa no pino do moedeiro, borda de subida
  EIFR |= (1 << INTF0); //Limpa o FLAG de interrupcao caso tenha acontecido alguma nesse meio tempo
  interrupts(); //volta a habilitar as interrupcoes
  Serial.begin(9600);
  Serial1.begin(9600);
//limpaEeprom();
  popularEstadoSensores(2);
  
  Wire.begin();
  rtc.begin();
  if (!rtc.isrunning()) {
    if(DEBUG) Serial.println(F("Init RTC"));
    //Ajusta RTC para a data/hora de compilacao
    rtc.adjust(DateTime(__DATE__, __TIME__));
  }

  if (!SD.begin(PIN_SD_CS) && DEBUG) {
    Serial.println(F("Erro SD"));
  }
  delay(15); //Espera >=11ms antes do primeiro comando para SHT

}
void loop(){  
  
  DEBUG = !digitalRead(PIN_DEBUG) || DEBUG_SOFT_FLAG;
  if(Serial.available()){ //Verifica se chegou algum comando valido pela Serial
    char tempChr = Serial.read();
    switch (tempChr) {
    case '#':
      showMenu();
      break;
    }
  }

  if(millis() > auxFuturo){
    logado=false;
    auxFuturo = millis() + (lerPeriodo()*1000);
    
    if(DEBUG){
      Serial.println();
      Serial.println(lerDataHora(1));
    }
    
    if(sensorSHT){//INICIO-IF sensorSHT
      sht.measTemp(&hTempData);
      temperatura=sht.calcTemp(hTempData);
      sht.measHumi(&hTempData);
      umidade = sht.calcHumi(hTempData, temperatura);
      if(DEBUG){
        Serial.print(F("Temp:(C) "));
        Serial.println(temperatura);
        Serial.print(F("Umid(%): "));
        Serial.println(umidade);
      }
    }//FIM-IF sensorSHT

    if(sensorBMP){
      if(bmp.begin()){
        bmpTemperatura = bmp.readTemperature();
        bmpPressao = bmp.readPressure();
        if(DEBUG){
          Serial.print(F("TempBMP:(C) "));
          Serial.println(bmpTemperatura);
          Serial.print(F("atm:(PA) "));
          Serial.println(bmpPressao);
        }
      }else if(DEBUG){
        Serial.println(F("Erro BMP085"));
      }
    }

    if(sensorLDR){//INICIO-IF LDR
      luminosidade = lerLdr();
      if(DEBUG){
        Serial.print(F("LDR:0-1000 "));
        Serial.println(luminosidade);
      }
    }//FIM-IF LDR 
    if(sensorPluv && DEBUG){
      Serial.print(F("Basculadas Pluv: "));
      Serial.println(contadorPluviometro);
    }

      String unixTime = lerDataHora(2);
      unixTime = unixTime.substring((unixTime.length()-8));
      char nomeArquivo[9];
      unixTime.toCharArray(nomeArquivo,sizeof(nomeArquivo));

      File dataFile = SD.open(nomeArquivo,FILE_WRITE);
      if(dataFile){
        dataFile.print("GET /update?key=SJPMZLADV8ORJO1W");
        dataFile.print("&created_at=");
        dataFile.print(lerDataHora(1));
        if(sensorSHT){
          dataFile.print("&1=");
          dataFile.print(temperatura);
          dataFile.print("&2=");
          dataFile.print(umidade);

        }
        if(sensorBMP){
          dataFile.print("&3=");
          dataFile.print(bmpPressao);
          dataFile.print("&4=");
          dataFile.print(bmpTemperatura);
        }
        if(sensorLDR){
          dataFile.print("&5=");
          dataFile.print(luminosidade);
        }
        if(sensorPluv){
          dataFile.print("&6=");
          dataFile.print(contadorPluviometro);
  
        }
        dataFile.println(" HTTP/1.0");
        dataFile.close();
        
        if(DEBUG) Serial.println(F("Salvo SD"));
      }else{
        if(DEBUG) Serial.println(F("Erro SD"));
      }
    contadorPluviometro = 0; //Sempre zera contador;

    digitalWrite(PIN_RELE_GPRS,RELE_LIGADO); //Habilita GPRS
    limpaBufferSerial1();
    delay(5000);
    Serial1.println("AT");
    Serial1.setTimeout(50000);
    boolean atSucesso=false;
    
    atSucesso = Serial1.findUntil("CONNECT","NO CARRIER");
    if(!atSucesso){
      Serial.println("SEM CONN");
      Serial1.setTimeout(5000);
      Serial1.println("AT");
      atSucesso = Serial1.findUntil("OK","ERROR");
      if(atSucesso){
        Serial1.setTimeout(10000);
        Serial1.println("ATD*99***1#");
        atSucesso = Serial1.findUntil("CONNECT","ERROR");
        if(!atSucesso){
          digitalWrite(PIN_RELE_GPRS,RELE_DESLIGADO);//Não conseguiu conectar, desliga modem
        }
      }
    }
    Serial.println("CONECTADO");
    root = SD.open("/"); //Abre diretotio RAIZ
    root.rewindDirectory();
    for(int nSend=0;nSend<20;nSend++){
      File entry = root.openNextFile();
      if(!entry){ //Não tem arquivos
        Serial.println("Sem Arquivo");
        break;//Sai do loop FOR
      }
      Serial.print("nSend:");
      Serial.println(nSend);
      if (!entry.isDirectory()) {//Verifica se não é diretorio
        File dataFile = SD.open(entry.name()); //Abre arquivo
        if (dataFile) { //Arquivo aberto com sucesso
          byte tempChr,contaChr;
          contaChr=0;
          boolean erroChr =false; //Variavel que ajuda a verificar se conteudo do Arquivo é valido
          
          if(!dataFile.available()) erroChr=true; //Arquivo vazio
          
          while (dataFile.available() && !erroChr && contaChr<3) {//le os 3 primeiros caracteres e verifica se o conteudo == GET
            tempChr=dataFile.read();
            contaChr++;
            if(contaChr==1 && tempChr!='G')
              erroChr=true;
            else if(contaChr==2 && tempChr!='E')
              erroChr=true;
            else if(contaChr==3 && tempChr!='T')
              erroChr=true;
          }
          if(!erroChr){

            limpaBufferSerial1();
            Serial1.print("GET");
            while(dataFile.available()){
              tempChr = dataFile.read();
              if(tempChr==13) break;
              Serial1.write(tempChr);
            }
            Serial1.println();
            Serial1.println();
            erroChr=Serial1.findUntil("200 OK", "ERROR");//Aproveita variavel erroChr. Se encontrar 200 OK teve sucesso no envio
            if(erroChr){//teve sucesso
               SD.remove(entry.name());//Apaga arquivo
               Serial.println("DEL OK. WAIT CONNECT");
               Serial1.setTimeout(15000);               
               atSucesso = Serial1.findUntil("CONNECT","ERROR");
            }
          }else{//Arquivo invalido
            SD.remove(entry.name());//Apaga arquivo invalido
          }//FIM IF que checa se arquivo é valido
        }//FIM IF que abriu Arquivo
        dataFile.close();
      }//FIM IF que verifica se é diretorio
      entry.close();
      delay(1000);
    }//FIM FOR que pega os proximos 5 arquivos    
    root.close();
    digitalWrite(PIN_RELE_GPRS,RELE_DESLIGADO); //DesligaGPRS
  }//FIM-IF MILLIS>FUTURO  

  
}

void showDataHora(){
  Serial.println(lerDataHora(1));
}
String lerDataHora(byte formato){
  DateTime _agora = rtc.now();
  String _retorno="";
  if(formato==1){
    _retorno += _agora.year();
    _retorno += '/';
    _retorno += _agora.month();
    _retorno += '/';
    _retorno += _agora.day();
    _retorno += ' ';
    _retorno += _agora.hour();
    _retorno += ':';
    _retorno += _agora.minute();
    _retorno += ':';
    _retorno += _agora.second();
  }else if(formato==2){
    _retorno=String(_agora.unixtime());
  }
  return _retorno;
}

int lerLdr(){
  return analogRead(PIN_LDR);
}
void showMenu(){
  char _opcao = 0;
  if(verificaLogin()){
    limpaBufferSerial();
    Serial.println();
    Serial.println();
    Serial.println(F("Menu:"));
    Serial.println(F("0-Sair"));
    if(DEBUG_SOFT_FLAG)
      Serial.println(F("1-Des.DEBUG"));
    else
      Serial.println(F("1-Hab.DEBUG"));
    Serial.println(F("2-Data/hora"));
    Serial.println(F("3-Senha"));
    Serial.println(F("4-Periodo medicao"));
    Serial.println(F("5-Resetar"));
    Serial.println(F("6-Teste SHT"));
    Serial.println(F("7-Teste PLUV"));
    Serial.println(F("8-Teste BMP"));
    Serial.println(F("9-Hab/Des sensores"));
    Serial.println(F("Digite o numero e tecle ENTER:"));
    aguardaChrSerial(15000); //aguarda 15 segundos ou ate que algo seja digitado
    if(Serial.available())
      _opcao = Serial.read();
    else
      _opcao = '0';
    Serial.write(_opcao);
    Serial.println();
    switch (_opcao) {
    case '1':
      showMenuAjustaDebug();
      break;
    case '2':
      showMenuAjustaDataHora();
      break;
    case '3':
      showMenuAlterarSenha();
      break;
    case '4':
      showMenuAjustaPeriodo();
      break;
    case '5':
      resetPlaca();
      break;
    case '6':
      showMenuTestaSHT();
      break;
    case '7':
      showMenuTestaPluviometro();
      break;
    case '8':
      showMenuTestaBMP();
      break;
    case '9':
      showMenuHabDesSensores();
      break;
    case '0':
      Serial.println(F("Saindo menu!"));
      break;
    }
  }else{
    Serial.println(F("Digite #senha para logar"));
  } 
}
boolean verificaLogin(){
  if(logado==false){
    int _aux=8; //8=endereço do primeiro caracter da senha na EEPROM
    logado = true;
    delay(10);
    while(Serial.available() && _aux<18){//17= endereço do ultimo caracter da senha na EEPROM
      byte _chrSerial = Serial.read();
      byte _chrEEPROM = EEPROM.read(_aux);
      delay(10);
      if(_chrSerial!=_chrEEPROM){
        logado=false;
      }
      _aux++;
    }
    for(_aux=_aux;_aux<18;_aux++){
      if(EEPROM.read(_aux)!=0) logado=false;
    }
  }
    return logado;
}
void showMenuAjustaDebug(){
  DEBUG_SOFT_FLAG = !DEBUG_SOFT_FLAG; 
  Serial.print(F("Debug: "));
  Serial.println(DEBUG_SOFT_FLAG);
}
byte popularEstadoSensores(byte _enderecoEEPROM){
  byte _estado=EEPROM.read(_enderecoEEPROM);
  sensorSHT = bitRead(_estado,0);
  sensorPluv = bitRead(_estado,1);
  sensorLDR = bitRead(_estado,2);
  sensorBMP = bitRead(_estado,3);
  if(DEBUG){
    Serial.print(F("SHT:"));
    Serial.print(sensorSHT);
    Serial.print(F(" Pluv:"));
    Serial.print(sensorPluv);
    Serial.print(F(" LDR:"));
    Serial.print(sensorLDR);
    Serial.print(F(" BMP:"));
    Serial.println(sensorBMP);
  }
  return _estado;
}
void showMenuAjustaDataHora(){
  byte _tempMes,_tempDia,_tempHora,_tempMin,_tempSeg;
  word _tempAno;
  Serial.print(F("Data/Hora:"));
  showDataHora();
  
  Serial.println(F("Digite ANO (Ex: 2014): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempAno = Serial.parseInt();
  if(_tempAno<2014 || _tempAno>2200){
    Serial.print(F("Ano INV"));
    return;
  }
  Serial.println(F("Digite MES (Ex: 03): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempMes = Serial.parseInt();
  if(_tempMes<1 || _tempMes>12){
    Serial.print(F("Mes INV"));
    return;
  }
  Serial.println(F("Digite DIA (Ex: 05): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempDia = Serial.parseInt();
  if(_tempDia<1 || _tempDia>31){
    Serial.print(F("Dia INV"));
    return;
  }
  Serial.println(F("Digite HORA (Ex: 13): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempHora = Serial.parseInt();
  if(_tempHora<0 || _tempHora>23){
    Serial.print(F("H INV"));
    return;
  }
  Serial.println(F("Digite MIN (Ex: 09): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempMin = Serial.parseInt();
  if(_tempMin<0 || _tempMin>59){
    Serial.print(F("Min INV"));
    return;
  }
  Serial.println(F("Digite SEG (Ex: 55): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  _tempSeg = Serial.parseInt();
  if(_tempSeg<0 || _tempSeg>59){
    Serial.print(F("Seg INV"));
    return;
  }
  rtc.adjust(DateTime(_tempAno,_tempMes,_tempDia,_tempHora,_tempMin,_tempSeg));
  delay(50);
  Serial.print(F("Data/Hora:"));
  showDataHora();
  
}
void showMenuAlterarSenha(){
  byte _aux = 8; //8=endereço do primeiro caracter da senha na EEPROM
  Serial.println(F("Senha (Max 10):"));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  
  while(Serial.available() && _aux<18){
    EEPROM.write(_aux,Serial.read());
    delay(10);
    _aux++;
  }
  for(_aux=_aux;_aux<18;_aux++){
    EEPROM.write(_aux,0);
  }
  Serial.println(F("Salvo"));
}

void showMenuAjustaPeriodo(){
  Serial.print(F("Periodo:"));
  Serial.println(lerPeriodo());
  
  Serial.println(F("Periodo em segundos(de 120 a 65000): "));
  limpaBufferSerial();
  aguardaChrSerial(15000); //Aguarda 15 segundos ou ate que algo seja digitado
  word _periodo = Serial.parseInt();
  if(_periodo>=120){
    salvaPeriodo(_periodo);
    Serial.print(F("Periodo: "));
    Serial.println(lerPeriodo());
  }else{
    Serial.println(F("Periodo INV"));
  }
}
void showMenuTestaSHT(){
  limpaBufferSerial();
  Serial.println();
  Serial.println(F("Digite 0 para sair"));
  while(!Serial.available()){
      sht.measTemp(&hTempData);
      temperatura=sht.calcTemp(hTempData);
      sht.measHumi(&hTempData);
      umidade = sht.calcHumi(hTempData, temperatura);
      Serial.print(temperatura);
      Serial.print(F(" C "));
      Serial.print(umidade);
      Serial.println(F(" %" ));
      delay(1000);
  }
}
void showMenuTestaBMP(){
  limpaBufferSerial();
  Serial.println();
  Serial.println(F("Digite 0 para sair"));
  while(!Serial.available()){
     if(bmp.begin()){
        bmpTemperatura = bmp.readTemperature();
        bmpPressao = bmp.readPressure();
        Serial.print(bmpTemperatura);
        Serial.print(F(" C-"));
        Serial.print(bmpPressao);
        Serial.println(F(" PA"));   
      }else{
        Serial.println(F("Erro BMP085"));
      }
      delay(1000);
  }
}
void showMenuTestaPluviometro(){
  limpaBufferSerial();
  contadorPluviometro = 0;
  Serial.println();
  Serial.println(F("Digite 0 para sair"));
  Serial.println(F("Aguardando PLUVIOMETRO"));
  while(!Serial.available()){
    if(contadorPluviometro>0){
      Serial.print(F("Basculada: "));
      Serial.println(contadorPluviometro);
      contadorPluviometro=0;
    }
  }
  
  
}
void showMenuHabDesSensores(){
  byte _estadoSensore=popularEstadoSensores(2);
  char _opcao = 0;

  limpaBufferSerial();
  Serial.println();
  Serial.println();
  Serial.println(F("Hab e Desab Sensores:"));
  Serial.println(F("0-Sair"));
  if(sensorSHT)
    Serial.println(F("1-Des SHT75"));
  else
    Serial.println(F("1-Hab SHT75"));
  if(sensorPluv)
    Serial.println(F("2-Des Pluviometro"));
  else
    Serial.println(F("2-Hab Pluviometro"));
  if(sensorLDR)
    Serial.println(F("3-Des LDR"));
  else
    Serial.println(F("3-Hab LDR"));
  if(sensorBMP)
    Serial.println(F("4-Des BMP085"));
  else
    Serial.println(F("4-Hab BMP085"));
  Serial.println(F("Digite a opcao e ENTER:"));
  aguardaChrSerial(15000); //aguarda 15 segundos ou ate que algo seja digitado
  if(Serial.available())
    _opcao = Serial.read();
  else
    _opcao = '0';
  Serial.write(_opcao);
  Serial.println();
  switch (_opcao) {
  case '1':
    if(sensorSHT)
      bitClear(_estadoSensore,0);
    else
      bitSet(_estadoSensore,0);
    break;
  case '2':
    if(sensorPluv)
      bitClear(_estadoSensore,1);
    else
      bitSet(_estadoSensore,1);
    break;
  case '3':
    if(sensorLDR)
      bitClear(_estadoSensore,2);
    else
      bitSet(_estadoSensore,2);
    break;
  case '4':
    if(sensorBMP)
      bitClear(_estadoSensore,3);
    else
      bitSet(_estadoSensore,3);
    break;
  case '0':
    Serial.println(F("Saindo"));
    break;
  }
  EEPROM.write(2,_estadoSensore);  
  delay(10);
  _estadoSensore=popularEstadoSensores(2);
}
void limpaBufferSerial(){
  delay(10); //aguarda 10 milisegundos para o buffer estabilizar
  while(Serial.available()){
    Serial.read(); //Apenas para limpar o buffer
    delay(10); //aguarda 10 milisegundos para o buffer estabilizar
  }
}
void limpaBufferSerial1(){
  delay(10); //aguarda 10 milisegundos para o buffer estabilizar
  while(Serial1.available()){
    Serial1.read(); //Apenas para limpar o buffer
    delay(10); //aguarda 10 milisegundos para o buffer estabilizar
  }
}
void aguardaChrSerial(unsigned long _tempoMili){
  delay(10); //aguarda 10 milisegundos para o buffer estabilizar
  _tempoMili += millis();
  while(!Serial.available() && millis()<_tempoMili);
}
unsigned long lerPeriodo(){
  unsigned long periodo;
  periodo = EEPROM.read(0);
  periodo = periodo << 8;
  periodo |= EEPROM.read(1);
  if(periodo<30)
    periodo=PERIODO_MEDICAO; //valor default
  
  return periodo;  
}
void salvaPeriodo(word _novoPeriodo){
  byte _auxPeriodo;
  _auxPeriodo = highByte(_novoPeriodo);
  EEPROM.write(0,_auxPeriodo);
  _auxPeriodo = lowByte(_novoPeriodo);
  EEPROM.write(1,_auxPeriodo);
  auxFuturo = millis() + (_novoPeriodo*1000);
}
void incrementaPluviometro(){
  if(millis()>(pluvDebounce+100)){
    contadorPluviometro++;
    pluvDebounce = millis();
  }
}

void limpaEeprom(){
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
  if(DEBUG){
  for (int i = 0; i < 512; i++)
    Serial.print(EEPROM.read(i));
  }
}
void resetPlaca() 
{
  asm volatile ("  jmp 0");  
} 
